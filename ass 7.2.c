#include <stdio.h>
int main() {
    char arr[1000], c;
    int count = 0;

    printf("Enter a string: ");
    gets(arr);

    printf("Enter a character to find its frequency: ");
    scanf("%c", &c);

    for (int i = 0; arr[i] != '\0'; ++i) {
        if (c == arr[i])
            ++count;
    }

    printf("Frequency of %c = %d", c, count);
    return 0;
}
